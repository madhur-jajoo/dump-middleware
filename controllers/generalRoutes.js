getMessage = function(req, res, next){
    console.log({"msg": "Get request hit"});
    next();
}

postMessage = function(req, res, next){
    // console.log(req.method, " method call");
    console.log({"msg": "Post request hit"});
    next();
}

putMessage = function(req, res, next){
    console.log({"msg": "Put request hit"});
    next();
}

deleteMessage = function(req, res, next){
    console.log({"msg": "Delete request hit"});
    next();
}

module.exports = {
    getMessage, postMessage, putMessage, deleteMessage
}