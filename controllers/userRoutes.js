const jwt = require('jwt-simple');

const config = require('./../config/config');
const User = require('./../models/user');

getUser = function(req, res, next){
    User.find()
        .then( (result) => {
            res.json(result);
            next();
        })
        .catch( (err) => console.error(err) )
}

postUser = function(req, res, next){
    let user = new User();
    user.username = req.body.username;
    user.password = req.body.password;

    user.save()
        .then( (result) => {
            console.log("User added");
            next();
        })
        .catch( (err) => console.error(err) )
}

logUser = function(req, res, next){
    User.findOne({ username: req.body.username }, function(err, user){
        if(err) throw err;
        else{
            user.comparePassword(req.body.password, function(err, isMatch){
                if(isMatch && !err){
                    console.log(user.username + " logged In");
                    let token = jwt.encode(user, config.secret);
                    res.json({"token":'JWT '+token});
                }else{
                    next();
                }
            })
        }
    })
    // next();
}


module.exports = {
    getUser, postUser, logUser
}