const express = require('express');

const routesController = require('./../controllers/generalRoutes');

const router = express.Router();

router.route('/')
    .get(routesController.getMessage)
    .post(routesController.postMessage)
    .put(routesController.putMessage)
    .delete(routesController.deleteMessage);

module.exports = router