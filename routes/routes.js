const express = require('express');

const UserRoutes = require('./userRoutes');
const GeneralRoutes = require('./generalRoutes');

const router = express.Router();

router.use('/', GeneralRoutes);
router.use('/user', UserRoutes);

module.exports = router