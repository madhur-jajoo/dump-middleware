const express = require('express');
const morgan = require('morgan');
const passport = require('passport');
require('passport-jwt');
require('jsonwebtoken');
const jwt = require('jwt-simple');

const config = require('./../config/config');
const User = require('./../models/user');

const router = express.Router();

router.get('/', passport.authenticate('jwt',{session: false}), function (req, res) {
    console.log("ahdfb");
    var token = getToken(req.headers);
    if (token) {
        var decoded = jwt.decode(token, config.secret);
        User.findOne({
            username: decoded.username
        }, function (err, user) {
            console.log(user);
            if (err) throw err;

            if (!user) {
                return res.status(403).send({ success: false, msg: 'Authentication failed. User not found.' });
            } else {
                res.json({ success: true, msg: 'Welcome in the member area ' + user.name + '!' });
            }
        });
    } else {
        return res.status(403).send({ success: false, msg: 'No token provided.' });
    }
});

getToken = function (headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

module.exports = router;