const express = require('express');
const morgan = require('morgan');
const passport = require('passport');


const userController = require('./../controllers/userRoutes');
const protectedRoutes = require('./protectedRoutes');

const router = express.Router();

require('./../middlewares/passport')(passport);

router.use(passport.initialize())
router.use(morgan('dev'));

router.route('/')
    .get(userController.getUser)
    .post(userController.postUser)

router.route('/login')
    .post(userController.logUser)

router.use('/protected', passport.authenticate('jwt',{session: false}), protectedRoutes);

module.exports = router