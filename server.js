const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const config = require('./config/config');
const routes = require('./routes/routes');
const URLLog = require('./middlewares/url-log');
const successLog = require('./middlewares/succes-log');

const app = express();

mongoose.Promise = global.Promise;
mongoose.connect(config.DB_URL)
    .then(() => console.log("Connected to database"))
    .catch((err) => console.error("Error connecting to the database..."));


app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(URLLog);
app.use('/api', routes);
app.use(successLog);


const PORT = config.PORT || 8080;
app.listen(PORT, function(err){
    if(err){console.err("Error starting the server...", err)}
    else{console.log("Server started on port " + PORT)}
});