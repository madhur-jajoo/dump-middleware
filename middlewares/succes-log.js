require('colors');

successLog = function(req, res, next){
    console.log(req.method + " : " + req.path + " Successfully Rendered ".green);
    res.end();
}

module.exports = successLog;