
URLLog = function(req, res, next){
    console.log("URL hit to : ", req.originalUrl);
    next();
}

module.exports = URLLog;